const http = require("http") 

const port = 4000

let users = [
{
	"name": "Lalisa",
	"email":	"lalisa@blackpink.com"
}, 
{
	"name": "Jisoo",
	"email": "jisoo@blackpink.com"

}]

 http.createServer((request, response) =>  {
	
		if(request.url == "/users" && request.method == "GET"){
			response.writeHead(200, {'Content-Type':'application/json'})

			//We convert the users array into JSON  since the server 

			response.write(JSON.stringify(users))
			response.end()
		}

		if(request.url == "/users" && request.method == "POST"){
			
			// 1. initialize request body varaiable which will later contain the data/body from postman: 

			let requestBody = ''
			
		
			// 2. Upon receiving the data re-assign value of requestBody to the contents of the body from the postman (NOTE: .on - detects if the data already entered the request): 

			request.on('data', (data) => { 
				requestBody += data
			})


			// 3. Before the request ends, convert the requestBody variable form string into Javascript object in order to be able to access its properties and assign them into newUser variable: 

			request.on('end', () => {
				console.log(typeof requestBody)
				requestBody = JSON.parse(requestBody)

				let newUser = {
					"name": requestBody.name, 
					"email": requestBody.email
				}


			// 4. After setting the values from the requestBody to the newUser variable, push the newUser variable into the users array: 

				users.push(newUser)

				response.writeHead(200,{'Content-Type':'application/json'})
				response.write(JSON.stringify(users))
				response.end()
			})
		}
	}).listen(port)


console.log(`Server is running at localhost:${port}`)