const http = require("http") 

const port = 4000

const server = http.createServer((request, response) =>  {
	
		if(request.url == "/" && request.method == "GET"){response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end('Welcome to Booking system');
		}
		else if(request.url == "/profile" && request.method == "GET"){response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end('Welcome to your Profile!');
		}	
		else if(request.url == "/courses" && request.method == "GET"){response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end('Here are our available courses');
		}
		else if(request.url == "/addCourse" && request.method == "POST"){response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end('Add course to our resources');
	}
	else if(request.url == "/updateCourses" && request.method == "PUT"){response.writeHead(200, {'Content-Type': 'text/plain'})
			response.end('Update a course to our resources');
	}
	else if (request.url === "/archiveCourses" && request.method === "DELETE"){
 		response.writeHead(200,{'Content-Type':'text/plain'});
 		response.end("Archive Courses to our resources");
 	}


	}).listen(port)


console.log(`Server is running at localhost:${port}`)